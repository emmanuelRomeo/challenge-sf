<?php

namespace ProductBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="accueil")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $entityManagerDefault = $this->get('doctrine.dbal.default_connection');
        $result = $em->getRepository('ProductBundle:Product')->loadData($entityManagerDefault);

        return $this->render('ProductBundle:Default:index.html.twig');
    }
}
