<?php

namespace ProductBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use ProductBundle\Entity\Marque;
use Symfony\Component\HttpFoundation\Request;
/**
 * Marque controller.
 *
 * @Route("marque")
 */
class MarqueController extends Controller
{
    /**
     *
     * @Route("/", name="marque_index")
     *
     */
    public function listeAction(){

        $em = $this->getDoctrine()->getManager();

        $marques = $em->getRepository("ProductBundle:Marque")->findAll();

        return $this->render('ProductBundle:Marque:list.html.twig', array(
            'marques' => $marques,
        ));
    }

    /**
     *
     * @Route("/add", name="marque_add")
     * @Route("/add/{id}", name="marque_show")
     */
    public function addAction(Request $request,$id = null){

        $em = $this->getDoctrine()->getManager();

        $isEdit = false;
        $marque = new Marque();
        if( !is_null($id) && $request->getMethod() == "GET"){
            $marque =  $em->getRepository('ProductBundle:Marque')->find($id);
            $isEdit = true;
        }


        if($request->getMethod() == "POST"){
            $data = $request->request ;

            if($data->get('id') != ''){ //la requette est une mise à jour
                $marque =  $em->getRepository('ProductBundle:Marque')->find($data->get('id'));
            }

            $marque = $this->save($marque,$data);
            $isEdit = true;
            $marques = $em->getRepository("ProductBundle:Marque")->findAll();

            return $this->render('ProductBundle:Marque:list.html.twig', array(
                'marques' => $marques,
            ));
        }
        $entityManagerDefault = $this->get('doctrine.dbal.default_connection');
        $fournisseurs = $em->getRepository("ProductBundle:Fournisseur")->getAllMarque($entityManagerDefault);

        return $this->render('ProductBundle:Marque:add.html.twig', array(
            'marque' => $marque,
            'fournisseurs' => $fournisseurs,
            'isEdit' => $isEdit
        ));
    }

    /**
     *
     * @Route("/delete/{id}", name="marque_delete")
     *
     */
    public function deleteAction($id){
        $em = $this->getDoctrine()->getManager();
        $marque =  $em->getRepository('ProductBundle:Marque')->find($id);

        $em->remove($marque);
        $em->flush($marque);

        $marques = $em->getRepository("ProductBundle:Marque")->findAll();
        return $this->render('ProductBundle:Marque:list.html.twig', array(
            'marques' => $marques
        ));
    }


    function save($marque ,$data){
        $em = $this->getDoctrine()->getManager();
        $ournisseur = $em->getRepository("ProductBundle:Fournisseur")->find($data->get('fournisseur_id'));
        $marque->setnom($data->get('nom'));
        $marque->setFournisseur($ournisseur);
        $em->persist($marque);
        $em->flush();
        return $marque;
    }
}
