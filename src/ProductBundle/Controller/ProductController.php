<?php

namespace ProductBundle\Controller;

use ProductBundle\Entity\Fiche;
use ProductBundle\Entity\FicheProduct;
use ProductBundle\Entity\Product;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use ProductBundle\Entity\Stock;
use \DateTime;


/**
 * Product controller.
 *
 * @Route("product")
 */
class ProductController extends Controller
{
    /**
     * Lists all product entities.
     *
     * @Route("/add", name="product_add")
     * @Route("/add/{id}", name="product_show")
     * @Method({"GET","POST"})
     */
    public function addAction(Request $request ,$id = null)
    {
        $em = $this->getDoctrine()->getManager();

        $product = new Product();
        if( !is_null($id) && $request->getMethod() == "GET"){
            $product =  $em->getRepository('ProductBundle:Product')->find($id);
        }

        $allfields = $this->getFieldListBox();
        $Fieldstypes = $allfields['types'];
        $Fieldsgenres = $allfields['genres'];
        $Fieldsmarques = $allfields['marques'];

        if($request->getMethod() == "POST"){
            $data = $request->request ;

           if($data->get('id') != ''){ //la requette est une mise à jour
               $product =  $em->getRepository('ProductBundle:Product')->find($data->get('id'));
           }

            $product = $this->save($product,$data);

            $stock = new Stock();
            $stock->setStock(0);
            $stock->setProductId( $product->getId());

            $em->persist($stock);
            $em->flush();

        }
        return $this->render('ProductBundle:Product:add.html.twig', array(
            'types' => $Fieldstypes,
            'genres' => $Fieldsgenres,
            'marques' => $Fieldsmarques,
            'product' => $product,
        ));
    }

    /**
     * Finds and displays a product entity.
     *
     * @Route("/delete/{id}", name="product_delete")
     * @Method("GET")
     */
    public function showAction($id)
    {

        $em = $this->getDoctrine()->getManager();
        $product =  $em->getRepository('ProductBundle:Product')->find($id);

        $em->remove($product);
        $em->flush($product);

        $products = $em->getRepository('ProductBundle:Product')->findAll();
        return$this->render('ProductBundle:Product:list.html.twig',array(
            'products' => $products,
        ));
    }

    /**
     *
     *
     * @Route("/", name="product_index")
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $entityManagerDefault = $this->get('doctrine.dbal.default_connection');
        $products = $em->getRepository('ProductBundle:Product')->getAllProduct($entityManagerDefault);
        return$this->render('ProductBundle:Product:list.html.twig',array(
            'products' => $products,
        ));
    }


    function getFieldListBox(){
        $em = $this->getDoctrine()->getManager();
         $types = $em->getRepository('ProductBundle:Marque')->findAll();
         $genres = $em->getRepository('ProductBundle:Type')->findAll();
         $marque = $em->getRepository('ProductBundle:Marque')->findAll();

        return array(
          "types" =>  $types,
          "genres" =>  $genres,
          "marques" =>  $marque,
        );
    }

    function save($product ,$data){
        $em = $this->getDoctrine()->getManager();
        $marque = $em->getRepository('ProductBundle:Marque')->find($data->get('marque'));
        $type_product= $em->getRepository('ProductBundle:Type')->find($data->get('type_product'));
        $genre_poduct = $em->getRepository('ProductBundle:Genre')->find($data->get('genre_product'));
//dump($marque);
//dump($type_product);
//dump($genre_poduct);die;
        $product->setTitre($data->get('titre'));
        $product->setDescription($data->get('description'));
        $product->setMarque($marque);
        $product->setPrixTtc($data->get('prix'));
        $product->setType($type_product);
        $product->setGenre($genre_poduct);
        $em->persist($product);
        $em->flush();
        return $product;
    }

    /**
     *
     * @Route("/stock/{id}", name="product_stock")
     * @Method("GET")
     */
    public function stockAction($id){
        $entityManagerDefault = $this->get('doctrine.dbal.default_connection');
        $em = $this->getDoctrine()->getManager();
        $product =  $em->getRepository('ProductBundle:Product')->getProductStock($entityManagerDefault,$id);
        return$this->render('ProductBundle:Product:stock.html.twig',array(
            'product' => $product[0],
        ));
    }

    /**
     *
     * @Route("/stock_change", name="stock_change")
     * @Method({"GET","POST"})
     */
    public function stockChangeAction(Request $request){
        $em = $this->getDoctrine()->getManager();

        $data = $request->request;
        if($data->get('stock_id') != ""){
            $stock = $em->getRepository('ProductBundle:Stock')->find($data->get('stock_id'));

            $stock->setStock($data->get('quantite'));
        }else{
            $stock = new Stock();
            $stock->setStock( $data->get('quantite'));
            $stock->setProductId( $data->get('id_product'));
        }

        $em->persist($stock);
        $em->flush();

        $entityManagerDefault = $this->get('doctrine.dbal.default_connection');
        $products = $em->getRepository('ProductBundle:Product')->getAllProduct($entityManagerDefault);
        return$this->render('ProductBundle:Product:list.html.twig',array(
            'products' => $products,
        ));
    }
    /**
     * enregistrement formulaire
     *
     * @Route("/commande", name="commande")
     * @Method({"GET","POST"})
     */
    public function commandeAction(Request $request){

        $data = $request->request;

        $em = $this->getDoctrine()->getManager();
        if($request->getMethod() == 'POST') {
            $client = $em->getRepository('ProductBundle:Client')->find($data->get('clientID'));
            $fiche = new Fiche();

            // save Fiche
            $fiche->setClient($client);
            $date = new DateTime();
            $fiche->setDate($date->getTimestamp());
            $emFiche = $this->getDoctrine()->getManager();
            $emFiche->persist($fiche);
            $emFiche->flush();

            $products = $data->get('dataProduct');
                // save fiche product
            foreach ( $products as $produt) {
               $emfiche_product = $this->getDoctrine()->getManager();
               $fiche_product = new FicheProduct();
               $fiche_product->setFiche($fiche);
               $produit = $em->getRepository('ProductBundle:Product')->find($produt['productId']);
               $fiche_product->setProduct($produit);
               $fiche_product->setQuantite($produt['quantite']);
               $emfiche_product->persist($fiche_product);
               $emfiche_product->flush();
                // update stock
                $stock = $em->getRepository('ProductBundle:Stock')->findBy(['productId'=>$produt['productId']])[0];
                $emStock = $this->getDoctrine()->getManager();
                $stockValue = intval($stock->getStock() ) - intval($produt['quantite']);
                $stock->setStock($stockValue);
                $emStock->persist($stock);
                $emStock->flush();

            }
            $entityManagerDefault = $this->get('doctrine.dbal.default_connection');
            $productsCommand = $em->getRepository('ProductBundle:Product')->getFicheClient($entityManagerDefault,$client->getId(),$fiche->getDate());
            return $this->render('ProductBundle:Product:resume.html.twig', array(
                "products" => $products,
                "client" => $em->getRepository('ProductBundle:Client')->find(1),
                "productsCommand" => $productsCommand,
                "isResume" => true
            ));
        }
        if(!isset($productsCommand)){
            $productsCommand = array();
        }

        $products = $em->getRepository('ProductBundle:Product')->findAll();
        return $this->render('ProductBundle:Product:fiche.html.twig', array(
            "products" => $products,
            "client" => $em->getRepository('ProductBundle:Client')->find(1),
            "productsCommand" => $productsCommand,
            "isResume" => false
        ));
    }
}
