<?php

namespace ProductBundle\Controller;

use ProductBundle\Entity\Fournisseur;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * Fournisseur controller.
 *
 * @Route("fournisseur")
 */
class FournisseurController extends Controller
{
    /**
     *
     * @Route("/", name="fournisseur_index")
     *
     */
    public function listeAction(){

        $em = $this->getDoctrine()->getManager();

        $fournisseurs = $em->getRepository("ProductBundle:Fournisseur")->findAll();

        return $this->render('ProductBundle:Fournisseur:list.html.twig', array(
            'fournisseurs' => $fournisseurs,
        ));
    }

    /**
     *
     * @Route("/add", name="fournisseur_add")
     * @Route("/add/{id}", name="fournisseur_show")
     */
    public function addAction(Request $request,$id = null){

        $em = $this->getDoctrine()->getManager();

        $fournisseur = new Fournisseur();
        if( !is_null($id) && $request->getMethod() == "GET"){
            $fournisseur =  $em->getRepository('ProductBundle:Fournisseur')->find($id);
        }


        if($request->getMethod() == "POST"){
            $data = $request->request ;

            if($data->get('id') != ''){ //la requette est une mise à jour
                $fournisseur =  $em->getRepository('ProductBundle:Fournisseur')->find($data->get('id'));
            }

            $fournisseur = $this->save($fournisseur,$data);
            $fournisseurs = $em->getRepository("ProductBundle:Fournisseur")->findAll();

            return $this->render('ProductBundle:Fournisseur:list.html.twig', array(
                'fournisseurs' => $fournisseurs,
            ));
        }


        return $this->render('ProductBundle:Fournisseur:add.html.twig', array(
            'fournisseur' => $fournisseur
        ));
    }

    /**
     *
     * @Route("/delete/{id}", name="fournisseur_delete")
     *
     */
    public function deleteAction($id){
        $em = $this->getDoctrine()->getManager();
        $fournisseur =  $em->getRepository('ProductBundle:Fournisseur')->find($id);

        $em->remove($fournisseur);
        $em->flush($fournisseur);

        $fournisseurs = $em->getRepository("ProductBundle:Fournisseur")->findAll();
        return $this->render('ProductBundle:Fournisseur:list.html.twig', array(
            'fournisseurs' => $fournisseurs
        ));
    }


    function save($fournisseur ,$data){
        $em = $this->getDoctrine()->getManager();

        $fournisseur->setnom($data->get('nom'));
        $fournisseur->setTel($data->get('tel'));
        $fournisseur->setAdresse($data->get('adresse'));
        $em->persist($fournisseur);
        $em->flush();
        return $fournisseur;
    }
}
