<?php

namespace ProductBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use ProductBundle\Entity\Client;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
/**
 * Client controller.
 *
 * @Route("client")
 */
class ClientController extends Controller
{
    /**
     * ajout client
     *
     * @Route("/formulaire", name="client_formulaire")
     * @Method({"GET"})
     */
    public function inscriptionAction(){
        return $this->render('ProductBundle:Client:formulaire.html.twig', array(
        ));
    }

    /**
     * enregistrement formulaire
     *
     * @Route("/add", name="client_add")
     * @Method({"POST"})
     */
    public function addAction(Request $request){

        $data = $request->request;

        $client = $this->save( new Client() ,$data);

        $em = $this->getDoctrine()->getManager();
        $products = $em->getRepository('ProductBundle:Product')->findAll();

        return $this->render('ProductBundle:Product:fiche.html.twig', array(
            "products" => $products,
            "client" => $client,
            "productsCommand" => array(),
            "isResume" => false
        ));
    }

    function save($client ,$data){
        $em = $this->getDoctrine()->getManager();

        $client->setNom($data->get('nom'));
        $client->setPrenom($data->get('prenom'));
        $client->setAdresse($data->get('adresse'));
        $client->setEmail($data->get('email'));
        $client->setTel($data->get('tel'));
        $em->persist($client);
        $em->flush();

        return $client;
    }


}
