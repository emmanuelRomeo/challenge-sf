<?php

namespace ProductBundle\Repository;


class FournisseurRepository extends \Doctrine\ORM\EntityRepository
{
    public function getAllMarque($entityManagerDefault)
    {
        $sql = "SELECT f.*
                FROM fournisseur as f
                where f.id not in (select fournisseur_id from marque)
                ";
        $products = $entityManagerDefault->fetchAll($sql);
        return $products;
    }
}