<?php

namespace ProductBundle\Repository;


class ProductRepository extends \Doctrine\ORM\EntityRepository
{
    public function getAllProduct($entityManagerDefault){
        $sql = "SELECT p.*,s.stock,m.nom as marque , t.nom as type , t.nom as genre
                FROM product as p
                left join stock  s on p.id = s.product_id
                left join marque  m on p.marque_id = m.id
                left join type t on p.type_id = t.id
                left join genre  g on p.genre_id = g.id
                ";
        $products = $entityManagerDefault->fetchAll($sql);
        return $products;
    }

    public function getProductStock($entityManagerDefault,$id){
        $sql = "SELECT p.*,s.stock,s.id as stock_id
                FROM product as p
                left join stock  s on p.id = s.product_id
                where p.id = " . $id ;
        return $entityManagerDefault->fetchAll($sql);
    }
    public function getFicheClient($entityManagerDefault,$idClient,$date){
        $sql = "SELECT p.titre , p.prix_ttc, fp.quantite ,  (p.prix_ttc * fp.quantite) as total ,
                c.nom,c.prenom,c.email,f.id,f.date
                FROM client as c
                inner join fiche f on c.id = f.client_id
                inner join fiche_product fp on f.id = fp.fiche_id
                inner join product p on fp.product_id = p.id
                where c.id = " . $idClient . " and f.date = " . $date ;
        return $entityManagerDefault->fetchAll($sql);
    }

    public function loadData($entityManagerDefault){

        $sql = "SELECT count(*) as nb from type ";
        $res = $entityManagerDefault->fetchAll($sql);
        if($res[0]['nb'] == 0){
            $sql = "INSERT INTO `type` (`id`, `nom`) VALUES(1, 'soleil'),(2, 'vue'),(3, 'sport')";
            $entityManagerDefault->query($sql);
            $sql = "INSERT INTO `genre` (`id`, `nom`) VALUES(1, 'homme'),(2, 'femme'),(3, 'mixte'),(4, 'enfant');";
            $entityManagerDefault->query($sql);
        }
        return true;
    }
}